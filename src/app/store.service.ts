import { Injectable } from '@angular/core';
import {LocalStorageService} from 'ng2-webstorage';
import {ArticleClass as Article} from './article-class';

class Store {

      public programs: Array<Article>;
      public exercises: Array<Article>;
      public cardios: Array<Article>;
      public recipes: Array<Article>;

      constructor() {

                  this.programs = [];
                  this.exercises = [];
                  this.cardios =  [];
                  this.recipes = [];
      }
}

@Injectable()
export class StoreService {

  public store: Store;


  constructor(private localStorage: LocalStorageService) {

      this.store = JSON.parse(this.localStorage.retrieve('store'))  || new Store();
      console.log(this.store);
  }

  public addNewProgram(program: Article): void {
        this.store.programs.unshift(program);
        this.saveStateToLocalStorage();
  }

  public addNewPractice(practice: Article): void {
        this.store.exercises.unshift(practice);
        this.saveStateToLocalStorage();
  }

  public addNewCardio(cardio: Article): void {
        this.store.cardios.unshift(cardio);
        this.saveStateToLocalStorage();
  }

  public addNewRecipe(recipe: Article): void {
        this.store.recipes.unshift(recipe);
        this.saveStateToLocalStorage();
  }

  private saveStateToLocalStorage(){
      this.localStorage.store('store', JSON.stringify(this.store));
  }


}
