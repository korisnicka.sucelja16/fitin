import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../store.service';
import {ArticleClass as Article} from './../article-class';

@Component({
  selector: 'app-cardio-details',
  templateUrl: './cardio-details.component.html',
  styleUrls: ['./cardio-details.component.css']
})
export class CardioDetailsComponent implements OnInit {

   private id: string;
   private cardio: Article;

  constructor(private router: Router, private storeService: StoreService) { }

  ngOnInit() {
    this.id =  this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
    this.cardio = this.storeService.store.cardios.find( (cardio) => cardio.id === this.id );
  }
}
