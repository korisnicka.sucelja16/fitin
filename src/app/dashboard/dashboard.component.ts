import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {LocalStorageService} from 'ng2-webstorage';
import { StoreService } from '../store.service';
import { AuthService } from './../auth.service';
import {ArticleClass as Article} from './../article-class';

class Store {

      public programs: Array<Article>;
      public exercises: Array<Article>;
      public cardios: Array<Article>;
      public recipes: Array<Article>;

      constructor() {

                  this.programs = [];
                  this.exercises = [];
                  this.cardios =  [];
                  this.recipes = [];
      }
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private showForm: boolean;
  private store: Store;
  private btnText: string;
  private programs: Array<Article>;
  private exercises: Array<Article>;
  private cardios: Array<Article>;
  private recipes: Array<Article>;

  constructor(private router: Router, private localStorage: LocalStorageService, private storeService: StoreService, private auth: AuthService) {
        this.showForm = false;
        this.btnText = 'Add new article';
        this.store = new Store();
   }

  ngOnInit() {
        this.showForm = false;

          if (!this.auth.isLoggedIn()) {
            this.router.navigate(['/home']);
          }

          this.programs = this.storeService.store.programs;
          this.exercises = this.storeService.store.exercises;
          this.cardios = this.storeService.store.cardios;
          this.recipes = this.storeService.store.recipes;

          this.store.programs = this.programs;
          this.store.exercises = this.exercises;
          this.store.cardios = this.cardios;
          this.store.recipes = this.recipes;
  }

  private toggleForm(){
    this.showForm = !this.showForm;
  }

  private showBtnText(): string {

     if (this.showForm){
      return 'Show all articles';
      } else {
        return 'Add new article';
      }
  }

  private deleteProgram(program: Article): void {
        let position = this.programs.indexOf(program);
        this.programs.splice(position, 1);
        this.saveStateToLocalStorage();
  }

  private deleteExercise(exercise: Article): void {
        let position = this.programs.indexOf(exercise);
        this.exercises.splice(position, 1);
         this.saveStateToLocalStorage();
  }

  private deleteCardio(cardio: Article): void {
        let position = this.cardios.indexOf(cardio);
        this.cardios.splice(position, 1);
        this.saveStateToLocalStorage();
  }

  private deleteRecipe(recipe: Article): void {
        let position = this.recipes.indexOf(recipe);
        this.recipes.splice(position, 1);
        this.saveStateToLocalStorage();
  }


  private saveStateToLocalStorage(){
      this.localStorage.store('store', JSON.stringify(this.store));
  }

}
