import { Component, OnInit } from '@angular/core';
import { StoreService } from './../store.service';
import {ArticleClass as Article} from './../article-class';

@Component({
  selector: 'app-cardio',
  templateUrl: './cardio.component.html',
  styleUrls: ['./cardio.component.css']
})
export class CardioComponent implements OnInit {

  private cardios: Array<Article>;
  private p: number;
  private filterText: string;

  constructor(private storeService: StoreService) {

  }

  ngOnInit() {
      this.filterText = 'Filter';
      this.cardios = this.storeService.store.cardios;
  }

  private oldest(): void {
    
        this.filterText = 'Najstarije';
        this.cardios = this.cardios.sort( (a, b) => {

              if (a.published < b.published) {
                return -1;
              }
              if (a.published > b.published) {
                return 1;
              }

             return 0;
        });
  }

  private newest(): void {

          this.filterText = 'Najnovije';
          this.cardios = this.cardios.sort( (a, b) => {

              if (a.published < b.published) {
                return 1;
              }
              if (a.published > b.published) {
                return -1;
              }

             return 0;
        });
  }

}
