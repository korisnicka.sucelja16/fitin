import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { StoreService } from '../store.service';
import { ArticleClass as Article} from './../article-class';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {

  private article: Article;
  private category: string;
  private title: string;
  private description: string;
  private content: string;
  private author: string;
  private picture: string;

  constructor(private router: Router, private store: StoreService, private toastr: ToastsManager, private vref: ViewContainerRef) {

      this.category = '';
      this.title = '';
      this.description = '';
      this.content = '';
      this.author = '';
      this.picture = '';

      this.toastr.setRootViewContainerRef(vref);
  }

  ngOnInit() {
  }

  private addNew(): void {

        this.article = new Article(this.title, this.description, this.content, this.author, this.picture);

        switch (this.category) {
              case 'programi': {
                    this.store.addNewProgram(this.article);
                    this.newArticleAlert();
              }
              break;
              case 'vjezbe': {
                    this.store.addNewPractice(this.article);
                    this.newArticleAlert();
              }
              break;
              case 'cardio': {
                    this.store.addNewCardio(this.article);
                    this.newArticleAlert();
              }
              break;
              case 'recepti': {
                    this.store.addNewRecipe(this.article);
                    this.newArticleAlert();
              }
              break;
              default: {
                  this.toastr.error('Something went wrong. Please try again!', 'Error!');
              }

        }

  }

  private newArticleAlert(): void {
       this.toastr.success('New article added!', 'Success!');
       this.resetForm();
  }

  private resetForm(): void {

      this.title = '';
      this.description = '';
      this.content = '';
      this.author = '';
      this.picture = '';

  }

}
