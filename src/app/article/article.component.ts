import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';
import {ArticleClass as Article} from './../article-class';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input('article') data: Article;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  private showDetails(): void  {

      let currentUrl = this.router.url;

      switch (currentUrl) {
            case '/programs': {
              this.router.navigate(['/programs/' + this.data.id]);
            }
            break;
            case '/cardios': {
              this.router.navigate(['/cardios/' + this.data.id]);
            }
            break;
      }
    }

}
