import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import {Ng2Webstorage} from 'ng2-webstorage';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { ModalModule } from 'ng2-bootstrap/modal';
import {Ng2PaginationModule} from 'ng2-pagination';
// Services
import { StoreService } from './store.service';
import { AuthService } from './auth.service';
// Routes
import { routes } from './app.routes';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgramsComponent } from './programs/programs.component';
import { ArticleFormComponent } from './article-form/article-form.component';
import { ArticleComponent } from './article/article.component';
import { ProgramDetailsComponent } from './program-details/program-details.component';
import { CardioComponent } from './cardio/cardio.component';
import { CardioDetailsComponent } from './cardio-details/cardio-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DashboardComponent,
    ProgramsComponent,
    ArticleFormComponent,
    ArticleComponent,
    ProgramDetailsComponent,
    CardioComponent,
    CardioDetailsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2Webstorage,
    Ng2PaginationModule,
    ToastModule.forRoot(),
    ModalModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  providers: [StoreService, AuthService],
  bootstrap: [AppComponent]
})

export class AppModule { }
