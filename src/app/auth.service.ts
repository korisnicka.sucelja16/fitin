import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  private isLogged: boolean;

  constructor() {
      this.isLogged = false;

  }

  public login(): void {
      this.isLogged = true;
  }

  public logout(): void {
      this.isLogged = false;
  }

  public isLoggedIn(): boolean {
      return this.isLogged;
  }

}
